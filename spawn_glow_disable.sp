#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <tf2>

public Plugin:myinfo =
{
name = "Spawn Glow Disable",
author = "Rowedahelicon",
description = "Removes the glow around players upon spawn",
version = "1.0",
url = "http://www.rowedahelicon.com"
}

public void TF2_OnConditionAdded(client, TFCond:condition)
{
    if (condition == 114)
    {
		TF2_RemoveCondition(client, 114);
    }
    return Plugin_Continue;
}